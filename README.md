# Learnathon Tracker

In this file we'll try to document what we're going to do, how we're learning and much more.

## The download, setup and installation part:

1. Download the community edition from here: [Visual Studio](https://visualstudio.microsoft.com/downloads/). Then during installation select asp .net and azure (and more if you want more). I'll useJetbrains Rider.
2. Download the stable version, windows 64 bit installer .msi file. [NodeJS](https://nodejs.org/en/download/) then nstall it. after that, type "npm version" in cmd or powershell to check if it's installed
3. If ok, Run this command "npm install -g @angular/cli" on your shell which will install Angular CLI. It will take time. if it fails, let me know. You need stable net connection for this btw. Type “Ng version” to check if it’s installed
4. To test if it works, go to your project directory and type "ng new first-app" you can change the name of app. You'll see a new folder with loads of subdirectory will be created. I prefer to opeen this one in VS Code to explore.
5. Download and install x64 installer(not binary): [Dotnet 5](https://dotnet.microsoft.com/en-us/download/dotnet/5.0) and Set path. run "dotnet --version" in cmd or powershell to check if you are successful. I installed dotnet version 6. Hopefully that's okay. If not, I'll switch version later
6. Download and install Rabbit MQ from here: [RabbitMQ](https://www.rabbitmq.com/install-windows.html#installer)
7. Open and account in bitbucket and jira both from atlassian.com
8. Create a repository in bitbucket, push some stuff from your local machine to test out how bitbucket works. Create a branch etc. bitbucket.org or atlassian.com
9. Install mongoshell and mongodb. [MongoDB](https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-windows/)
